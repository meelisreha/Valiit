package main.java;


public class GeenApp {

    public static void main(String[] args) {
        System.out.println(newGene());

    }


    public static String newGene() {
        Alleel alleel1 = new Alleel("reesus", true);
        Alleel alleel2 = new Alleel("reesus", false);

        Geen mother = new Geen(alleel1, alleel2);
        Geen father = new Geen(alleel1, alleel2);
        Alleel motherRandomAlleel = mother.randomAlleel();
        Alleel fatherRandomAlleel = father.randomAlleel();
        Geen newGeen = new Geen(motherRandomAlleel, fatherRandomAlleel);

        return "New gene was created: with name reesus and geen  being reesus: " + newGeen.checkReesus(motherRandomAlleel, fatherRandomAlleel) + "\n alleel1: " + motherRandomAlleel.isReesus() + "\n alleel2: " + fatherRandomAlleel.isReesus();
    }

}
