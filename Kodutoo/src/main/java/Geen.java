package main.java;


import java.util.Random;

public class Geen {

    private Alleel alleel1;
    private Alleel alleel2;


    public boolean checkReesus(Alleel alleel1, Alleel alleel2) {
        if (alleel1.isReesus() == true || alleel2.isReesus() == true) {
            return  true;
        } else {
            return false;
        }
    }

    public Geen(Alleel alleel1, Alleel alleel2) {
        this.alleel1 = alleel1;
        this.alleel2 = alleel2;
    }


    public Alleel randomAlleel() {
        Random rnd = new Random();
        if (rnd.nextBoolean()) {
            return alleel1;
        } else {
            return alleel2;
        }
    }

    @Override
    public String toString() {
        return String.format("%s, %s" , alleel1, alleel2);
    }
}
