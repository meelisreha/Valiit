package test.java;

import main.java.Alleel;
import main.java.Geen;

import static org.junit.Assert.*;

public class GeenTest {


    @org.junit.Test
    public void checkReesus() {
        Alleel alleelTestOne = new Alleel("TestOne", true);
        Alleel alleelTestTwo = new Alleel("TestTwo", false);
        Alleel alleelTestTree = new Alleel("TestTree", false);

        Geen geen1 = new Geen(alleelTestOne, alleelTestTwo);
        assertTrue( geen1.checkReesus(alleelTestOne, alleelTestTwo)); // kontrollin, et kui kas false ja true annab kokku true
        assertFalse(geen1.checkReesus(alleelTestTree, alleelTestTwo)); // kontrollin, et kas kaks false annab kokku false;

    }
}

