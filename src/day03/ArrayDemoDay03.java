package day03;

import java.lang.reflect.Array;
import java.util.Arrays;

public class ArrayDemoDay03 {
    public static void main(String[] args) {

        int[] number1 = {1, 2, 3};
        int[] number2 = {4, 5, 6, 7};
        int[] number3 = {8, 9};
        int[][] numbers = {
                number1,
                number2,
                number3,
        };
        for (int[] numbersid : numbers) {
            for (int numberis1 : numbersid) {
                System.out.println(numberis1);

            }
//            System.out.println();
        }


        String[] user = {"Mart", "Kelluke", "Tallinn"};
        String[][] users = {
                {"Marek", "Kuusk", "Tallinn", "Eesti"},
                {"Leida", "Kuusik", "Tartu"},
                {"Tanel", "Tohila", "Elva"},
                user
        };

        String[][] users2 = new String[3][3];
        users2[0][0] = "Marek";
        users2[0][1] = "Lints";
        users2[0][2] = "Tallinn";
        users2[1] = new String[]{"Tanel", "Tuisk", "Elva"};
        users2[2] = new String[]{"Proovikas", "Kool", "Emmaste"};
//        System.out.println(Arrays.toString(users2));

        for (String[] userid : users2) {
            for (String userData : userid) {
                System.out.print(userData + " ");
            }
            System.out.println();
        }

    }

}
