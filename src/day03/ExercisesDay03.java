package day03;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class ExercisesDay03 {
    public static void main(String[] args) {
        String city = args[0];
        doExercise2(city);
        int mark = new Integer(args[1]);
        int age = new Integer(args[2]);
        int num[] = new int[5];
        num[0] = 1;
        num[1] = 2;
        num[2] = 3;
        num[3] = 4;
        num[4] = 5;
        String[] cities = {"Tallinn", "Helsinki", "Madrid", "Paris"};
        System.out.println(Arrays.toString(cities));
        System.out.println(cities[1]);
        int levels[][] = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        for (int[] numbersid : levels) {
            for (int numberis1 : numbersid) {
                System.out.print(numberis1 + " ");
            }
            System.out.println();
        }

        String[][] cities2 = new String[3][4]; // sama mis all
        cities2 [0] = new String[] {"Tallinn", "Tartu", "Valga", "Võru"};
        cities2 [1] = new String[] {"Stockholm", "Uppsala", "Lund", "Köping"};
        cities2 [2] = new String[] {"Helsinki", "Espoo", "Hanko", "Jämsä" };

        for (String[] citikas : cities2) {
            for (String citikas1 : citikas){
                System.out.print(citikas1 + " ");
            }
            System.out.println();
        }

        String[][] cities3 = { // sama mis üleval
                {"Tallinn", "Tartu", "Valga", "Võru"},
                {"Stockholm", "Uppsala", "Lund", "Köping"},
                {"Helsinki", "Espoo", "Hanko", "Jämsa"},
        };
        for (String[] proov : cities3) {
            for (String trükitav : proov){
                System.out.print(trükitav + " ");
            }
            System.out.println();
        }



//        System.out.println(Arrays.toString((levels)));

        doExercise3(mark);
        doExercise4(mark);
        doExercise5(mark);
        doExercise6(age);
        doExercise7(num);

    }

    public static void doExercise2(String city) {
        if ("Milano".equalsIgnoreCase(city)) {
            System.out.println("Ilm on soe. ");
        } else {
            System.out.println("Ilm polegi kõige tähtsam. ");
        }
    }

    public static void doExercise3(int mark) { //Ise tehtud, töötab samamoodi kui alumine

//        int mark = 3;
        if (mark == 1) {
            System.out.println("nõrkadele");
        } else if (mark == 2) {
            System.out.println("rumalatele");
        } else if (mark == 3) {
            System.out.println("laiskadele");
        } else if (mark == 4) {
            System.out.println("vapsee");
        } else {
            System.out.println("tuupuritele");
        }
    }

    public static void doExercise4(int mark) {
        if (mark == 1) {
            System.out.println("nõrk");

        } else if (mark == 2) {
            System.out.println("mitterahuldav");
        } else if (mark == 3) {
            System.out.println("rahuldav");
        } else if (mark == 4) {
            System.out.println("hea");

        } else if (mark == 5) {
            System.out.println("väga hea");
        }

    }

    public static void doExercise5(int mark) {
        switch (mark) {
            case 1:
                System.out.println("nõrk");
                break; // võiks ka return
            case 2:
                System.out.println("mitterahuldav");
                break;
            case 3:
                System.out.println("rahuldav");
                break;
            case 4:
                System.out.println("hea");
                break;
            case 5:
                System.out.println("väga hea");
                break;
            default:
                System.out.println("Puudus");

        }
    }

    public static void doExercise6(int age) {
        System.out.println(age > 100 ? "Vana" : (age == 100) ? "Peaaegu vana" : "noor");
    }

    public static void doExercise7(int num[]) {
        System.out.println(String.format("%s %s %s ", num[0], num[2], num[num.length - 1]));
    }




}


