package day03;

public class IfIfElseSwitchDay03 {
    public static void main(String[] args) {

        int i = Integer.parseInt(args[0]);
 /*       boolean oddityCheck = i % 2 == 0;

        if (oddityCheck) { // refactor - ctrl+alt+shit+t - variable
            System.out.println("Etteantud arv " + i  + " on paaris");
        } else {
            System.out.println("Etteantud arv " + i  + " on paaritu");
        }
*/
        int inlineIFResult2 = (i % 2 == 0) ? 789 : 788;

        String InlineIfresult = (inlineIFResult2 % 2 == 0) ? // Tingimus
                "Etteantud arv " + i + " on paaris" // Kui tõene
                :
                "Etteantud arv " + i + " on paaritu";


        System.out.println( // If inline - otse prindib, saab kasutada väga lihtsatel juhtudel - TINGIMUSLAUSE
                InlineIfresult // Kui vale
        );

        int doorIsOpen = 5;

        switch (doorIsOpen) {
            case 0:
                System.out.println("Door is closed");
                break; // kui break ei ole siis läheb edasi, kui on break teeb ainult seda käsklust
            case 1:
                System.out.println("Door is a little bit open");
                break;
            case 2:
                System.out.println("Door is a almost open");
                break;
            case 3:
                System.out.println("Door is a very open");
                break;
            case 4:
                System.out.println("Door is a gone gone");
                break;
            case 5:
                System.out.println("What door? ");
            default:
                System.out.println("Who are you?");
        }

///*      Töötab 3 varianti
//        1. Variant - Proovib mõlemat ja kui üks neist on siis töötab.
 /*       String colour = "red";
          switch (colour) {
            case "Green":
            case "green":
                System.out.println("Driver can drive a car");
                break;
            case "Yellow":
            case "yellow":
                System.out.println("Driver has to be ready to stop the car or to start driving");
                break;
            case "Red":
            case "red":
                System.out.println("Driver has to stop car and wait for green light");
                break;
*/
/*      2. Variant - Muudame switchi sees lowercaseks ja võrdleme
        String colour = "green";
        switch (colour.toLowerCase()) {
            case "green":
                System.out.println("Driver can drive a car");
                break;
            case "yellow":
                System.out.println("Driver has to be ready to stop the car or to start driving");
                break;
            case "red":
                System.out.println("Driver has to stop car and wait for green light");
                break;
/**/
/*
//      3. Variant - Muudame stringi uppercase ja siis võrdleme.
        String colour = "green".toUpperCase();
        switch (colour) {
            case "GREEN":
                System.out.println("Driver can drive a car");
                break;
            case "YELLOW":
                System.out.println("Driver has to be ready to stop the car or to start driving");
                break;
            case "RED":
                System.out.println("Driver has to stop car and wait for green light");
                break;

*/
//  4. Variant - IF-iga

/*        String color = "RED";
        if ("Green".equalsIgnoreCase(color)) {
            System.out.println("Driver can drive a car");
        } else if ("Yellow".equalsIgnoreCase(color)) {
            System.out.println("Driver has to be ready to stop the car or to start driving");
        } else if ("Red".equalsIgnoreCase(color)) {
            System.out.println("Driver has to stop car and wait for green light");
        } else {
            System.out.println("There is no car");

        }*/




    }
}
