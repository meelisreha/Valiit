package day03;

import java.util.Scanner;

public class StringExercsisesDay03 {

    public final static double VAT_PERCENTAGE = 0.2d;

    public static void main(String[] args) { // meetod 1 - void - ei tagasta midagi
/*
        StringBuilder sb = new StringBuilder(); // uus muutuja on Stringbuilder! käsklus ütleb, et peab olema seda sorti muutuja
        String president1 = "Kontsantin Päts";
        String president2 = "Lennart Meri";
        String president3 = "Arnold Rüütel";
        String president4 = "Toomas Hendrik Ilves";
        String president5 = "Kersti Kaljulaid";

        sb.append(president1) // üks suur käsklus, nii kaua kui ; ei ole on sama käsk. Append = lisa.
                .append(", ")
                .append(president2)
                .append(", ")
                .append(president3)
                .append(", ")
                .append(president4)
                .append(" ja ")
                .append(president5)
                .append(" on Eesti presidendid.");

        System.out.println(sb.toString()); // teeme sb.toString() vastuse kohe stringiks(ehitatakse stringiks, muidu on tähtedejaga). Tuleb kindlalt tekst. Võiks ka lihtsalt System.out.println(sb);

        String[] stringArray1 = {"text1", "text2", "text3"};
//        Juhul kui on teadmata arv stringe arrays saab teha niimoodi

        String resultStr1 = ""; //
        StringBuilder sb2 = new StringBuilder(); // Kasutab stringbuilderit et ehitada stringi - Loe veel stringbuilderi kohta!!
        for (String item : stringArray1) {
            sb.append(item);
//            resultStr1 = resultStr1 + item; // Loob uue stringi iga kord
        }
        System.out.println(sb.toString());
//        StringBuilderi ülesanne on ehitada keerulisi stringe kokku ja eriti hea tsükli sees
//        StringBuilder = StringBuffer ehk sarnane asi aga natuke erinevad, StringBuilder on parem samas, StringBuffer on turvalisem

        String bookTitle = "Rehepapp";
//        String text1 = "Rammatu pealkiri on \"" + bookTitle + "\".";
//        System.out.println(text1);


        printSentence("Rehepapp"); //kutsun välja meetodit koos siseparameetriga!
        printSentence("Kollased Laevad"); //kutsun välja meetodit koos siseparameetriga!
        printSentence("Head õunad"); //kutsun välja meetodit koos siseparameetriga!
        printSentence("Erinevad naljad"); //kutsun välja meetodit koos siseparameetriga!
        printSentence("Must karp"); //FUNKTSIOONI VÄLJAKUTSE

//        doExercise2(); // FUNKTSIOONI VÄLJAKUTSUMINE

//      Sama moodi töötavad INTEGER on wrapper ja saab kasutada kui primitiiv muutujat i2. on palju valikuid, kuid i. ei üldse valikuid.
        int i = 7;
        Integer i1 = 7;
        Integer i2 = new Integer(7);
        i2.toBinaryString(7);
        int i3 = i + i2;
        System.out.println(i2.toBinaryString(7));
        System.out.println(Integer.toBinaryString(7));

//        Parsimine ehk teisendamine = parse klassid - muudavad tekstid teistsugusteks muutujateks ehk sõne "4" - int või byte-ks. Parimad on .parseINT() või .toString()

        System.out.println(args[0]);
        System.out.println(args[1]);

        int in1 = Integer.parseInt(args[0]);
        float in2 = Float.parseFloat(args[1]); // ütlen, et argumendid on float ja int
        float sum = in1 + in2;
        System.out.println(sum);

        String sumStr = ((Float) sum).toString(); // Teen tagasi numbrid stringiks
        String sumStr2 = String.valueOf(sum); // Muudab teised tüübid Stringiks. Soovitatv primitiivse puhul.

        System.out.println(sumStr);

//        String split meetodi näide
        String str3 = "tekst1, tekst2, tekst3, tekst4, tekst5";
        String[] str3Array = str3.split(", "); // splitiga lõhun ära ühe stringi juppideks vastavalt etteantud reeglile ", " ja paneme str3Array massiivi
        for (String strItem : str3Array) {
            System.out.println(strItem);
        }

        double price = 100.00d;
        price = price + price * VAT_PERCENTAGE;
        System.out.println(price);



*/



    }

    //  FUNKTSIOONI DEFINITISIOON - DEFINEERIMINE!
 /*   private static void printSentence(String bookTitle) { // meetod 2 - meetodi sees ei tohi olla teist meetodi sees, peab olema classi sees, kuid mitte meetodi
        String text1 = "Raamatu pealkiri on \"" + bookTitle + "\".";
        System.out.println(text1);
    }*/

/*    private static void doExercise2() {
//        String s1 = "Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida.";
        Scanner sc1 = new Scanner(System.in); // Scanner on hea stringi töötluse jaoks.
        sc1.useDelimiter("Rida: "); // ütlen talle milline muster on, et saaks aru,kus ridavahed on
        while (sc1.hasNext()) {
            System.out.println(sc1.next());

                    }

    }*/
}


