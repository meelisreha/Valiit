package day02;


public class StringDay02 {
    public static void main(String[] args) {
/*
        float muutuja1 = 456.78f; // f lõppu double - d lõppu
        String muutuja2 = "visitorsList";
        boolean muutuja3 = true;
//      Variant 1
        int[] muutuja4 = {5, 91, 304, 405};
//      Variant 2
//        int [] muutuja4 = new int[4];
//        numarray1[0] = 5;
//        numarray1[1] = 91;
//        numarray1[2] = 304;
//        numarray1[3] = 405;
        for (int i = 0; i < muutuja4.length; i++) {
            System.out.println(muutuja4[i]);
        }

        float[] muutuja5 = {56.7f, 45.8f, 91.2f};

//      Muutujad mis hoiavad väärtust a
//        String muutuja6 = "a";
//        String muutuja 7 = "a"
        char muutuja6 = 'a';
        char muutuja7 = 'a';
        String[] muutuja8 = {"See on esimene väärtus ", "67", "58.92"};
//      Suur number
        BigInteger muutuja9 = new BigInteger("7676868683452352345324534534523453245234523452345234523452345");
//      BigDecimal muutuja9 = new BigDecimal("7676868683452352345324534534523453245234523452345234523452345l");


        System.out.println(muutuja1);
        System.out.println(muutuja2);
        System.out.println(muutuja3);
        System.out.println(Arrays.toString(muutuja4));
        System.out.println(Arrays.toString(muutuja5));
        System.out.println(muutuja6);
        System.out.println(muutuja7);
        System.out.println(Arrays.toString(muutuja8)); // Vaja panna Arrays.toString() - muidu ei tule konkreetne vastusS
        System.out.println(muutuja9);

*/
        System.out.println("Hello, World! ");
        System.out.println("Hello 'World'!  ");
        System.out.println("Hello \"World\"!  ");
        System.out.println("Steven Hawking once said: \"Life would be tragic if it weren’t funny\".");
        System.out.println("Kui liita kokku sõned \"See on teksti esimene pool\" ” ning \"See on teksti teine pool\", siis tulemuseks saame \"See on teksti esimene pool See on teksti teine pool\". ");

//        String tallinnPopulation = "450 000";
        int tallinnPopulation = 450000;
        System.out.println("Tallinnas elab " + tallinnPopulation + " inimest");

        String bookTitle = "Rehepapp";
        System.out.println("Raamatu \"" + bookTitle + "\" Autor on Andrus Kivirähk.");

        String planet1 = "Merkuur";
        String planet2 = "Veenus";
        String planet3 = "Maa";
        String planet4 = "Marss";
        String planet5 = "Jupiter";
        String planet6 = "Saturn";
        String planet7 = "Uran";
        String planet8 = "Neptuun";
        int planetCount = 8;
        System.out.println(planet1 + ", " + planet2 + ", " + planet3 + ", " + planet4 + ", " + planet5 + ", " + planet6 + ", " + planet7 + " ja " + planet8 + " on Päikesesüsteemi " + planetCount + " planeeti");
        System.out.println(String.format("%s, %s, %s, %s, %s, %s, %s ja %s on Päikesesüsteemi %s planeeti.", planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8, planetCount)); // samamoodi saab teha






        String st1 = "Berlin";
        if (st1 == "Milano") {
            System.out.println("Ilm on soe");
        } else {
            System.out.println("Ilm polegi kõige tähtsam!");
        }


        int int1 = Integer.parseInt(args[0]);
//        int int1 = 3;
        if (int1 == 1) {
            System.out.println("nõrk");
        } else if (int1 == 2) {
            System.out.println("mitterahuldav");
        } else if (int1 == 3) {
            System.out.println("rahuldav");
        } else if (int1 == 4) {
            System.out.println("hea");
        } else {
            System.out.println("väga hea");


        }
    }
}

