package Exercises;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Nums {


    public static void main(String[] args) throws IOException {

  /*      List<String> newList = new ArrayList<>();

        Path path = Paths.get(args[0]);
        List<String> filesLines = Files.readAllLines(path);
        for (String fileLine : filesLines) {
            String lineParts = fileLine;
            newList.add(lineParts);
        }
        for (int i = 0; i < newList.size() - 1; i++) {



        char[] esimene = newList.get(i).toCharArray();
        char[] teine = newList.get(i+1).toCharArray();
        int add = 0;

        for (int j = newList.size() - 1; j > 1; j--) {
            int a = Character.getNumericValue(esimene[j]);
            int b = Character.getNumericValue(teine[j]);
            int c = (a + b + add) % 10;
            add = (a + b + add - c) / 10;


        }
        newList.add(Integer.toString(add));


//        for(int i = 60; i > 0; i--){
//            System.out.println(i);


        BigInteger big1 = new BigInteger("648949689396999848781911817076973155576849197092442811518681");
        BigInteger big2 = new BigInteger("174176960389006372374682745891425675445839235167557424684351");

        BigInteger big3 = big2.add(big1);
        System.out.println(big3);

    }*/

        List<String> numbers = new ArrayList<>();

        Path path = Paths.get(args[0]);
        List<String> fileLines;
        fileLines = Files.readAllLines(path);
        for (String fileLine : fileLines) {
            numbers.add(fileLine);
        }

        String finalNumber = "";
        int tempNum = 0;
        for (int j = numbers.get(1).length() - 1; j >= 0; j--) {
            for (int i = 0; i <= numbers.size() - 1; i++) {
                int x = Integer.parseInt(numbers.get(i).substring(j, j + 1));
                tempNum = tempNum + x;
            }
            int reminder = tempNum / 10;
            int digit = tempNum % 10;
            finalNumber = Integer.toString(digit) + finalNumber;
            tempNum = reminder;
        }
        finalNumber = Integer.toString(tempNum) + finalNumber;
        System.out.println(finalNumber);
    }

}



