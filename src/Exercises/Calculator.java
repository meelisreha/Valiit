package Exercises;


public class Calculator {
    public static void main(String[] args) {
        System.out.println(convertName("Burroughs")); // "BUR-9hs"

        System.out.println(convertName("abc")); // "ABC-3bc"

        System.out.println(convertName("")); // "ERROR"

        System.out.println(addition(2, 3)); // "2 + 3 = 5

        System.out.println(subtraction(3, 1)); // "3 - 1 = 2"

        System.out.println(repeat("a", 3)); // "aaa"

        System.out.println(line(4, true));

        System.out.println(center("abcde", 2, LongerPad.LEFT));

    }


    private enum LongerPad {LEFT, RIGHT }

    /**
     * Return a string following a naming convention.
     * <p>
     * [first three letters in uppercase]-[length of string][last two letters of string in lowercase]
     * <p>
     * If length of string is less than 3, return "ERROR".
     * <p>
     * <p>
     * <p>
     * //     * @param s original name
     */

    private static String convertName(String s) {
        if (s.length() < 3) {
            System.out.println("ERROR");
        } else {
            String s1 = (s.substring(0, 1).toUpperCase() + s.substring(1, 2).toUpperCase() + s.substring(2, 3).toUpperCase() + "-" + s.length() + s.substring(s.length() - 2, s.length() - 1).toLowerCase() + s.substring(s.length() - 1, s.length()).toLowerCase());
            System.out.println(s1);
        }


        return "";

    }

    /**
     * Return an expression that sums the numbers a and b.
     * <p>
     * Example: a = 3, b = 7 -> "3 + 7 = 10"
     */

    private static String addition(int a, int b) {
        int sum = a + b;
        System.out.println(a + " + " + b + " = " + sum);

        return "";

    }


    /**
     * Return an expression that subtracts b from a.
     * <p>
     * Example: a = 3, b = 1 -> "3 - 1 = 2"
     */

    private static String subtraction(int a, int b) {
        int sum = a - b;
        System.out.println(a + " - " + b + " = " + sum);

        return "";

    }


    /**
     * Repeat the input string n times.
     */

    private static String repeat(String s, int n) {
        for (int i = 0; i < n; i++) {
            System.out.print(s);
        }

        return "";

    }


    /**
     * Create a line separator using "-". Width includes the decorators if it has any.
     *
     * @param width     width of the line, which includes the decorator, if it has one
     * @param decorated if True, line starts with ">" and ends with "<", if False, consists of only "-"
     *                  <p>
     *                  If decorated and width is 1, return an empty string ("").
     */

    private static String line(int width, boolean decorated) {
        if (decorated && width == 1) {
            return "";
        } else {
            if (decorated = true) {
                System.out.print(">");
                for (int i = 0; i < width; i++) {
                    System.out.print("-");
                }
                System.out.println("<");
            } else {
                for (int i = 0; i < width; i++) {
                    System.out.print("-");
                }

            }
        }
        return "";


    }


    /**
     * Create a line separator using "-".
     *
     * @param width width of the line.
     */

    private static String line(int width) {
        for (int i = 0; i < width; i++) {
            System.out.print("-");


        }
        return "";
    }


    /**
     * Center justify a string.
     * "a", 3, LongerPad.LEFT -> " a "
     * When the string does not fit exactly:
     * If LongerPad.LEFT make the left padding longer, otherwise the right padding should be longer.
     * "ab", 5, LongerPad.LEFT  -> "  ab " "..ab."
     * "ab", 5, LongerPad.RIGHT -> " ab  " ".ab.."
     * If the width is smaller than the length of the string, return only the center part of the text.
     * "abcde", 2, LongerPad.LEFT  -> "bc" or "cd" (both are fine)
     * Return an empty string ("") if the width is negative.
     *
     * @param s     string to center
     * @param width width of the outcome
     * @param pad   left longer if LongerPad.LEFT, pad right longer if LongerPad.RIGHT
     * @return new center justified string
     */

    private static String center(String s, int width, LongerPad pad) {
        if (width < 0) {
            return "";
        } else {
            if (s.length() > width) {
                String s1 = s.substring(1, s.length() - width);
                for (int i = 0; i < width / 2; i++) {
                    System.out.print(" ");
                }
                System.out.print(s1);
                for (int i = 0; i < width / 2; i++) {
                    System.out.print(" ");
                }
            } else {
                if (width - s.length() % 2 != 0) {
                    if (pad == LongerPad.LEFT) {
                        for (int j = 0; j < width / 2 - 1; j++) {
                            System.out.print(" ");
                        }
                        System.out.print(s);
                        for (int j = 0; j < width / 2; j++) {
                            System.out.print(" ");
                        }
                    } else {
                        for (int j = 0; j < width / 2; j++) {
                            System.out.print(" ");
                        }
                        System.out.print(s);
                        for (int j = 0; j < width / 2 - 1; j++) {
                            System.out.print(" ");
                        }

                    }
                } else {


                    for (int i = 0; i < width / 2; i++) {
                        System.out.print(" ");
                    }
                    System.out.print(s);
                    for (int i = 0; i < width / 2; i++) {
                        System.out.print(" ");
                    }
                }
            }

        }
        System.out.println();

        return "";

    }


    /**
     * Create a string representing the display.
     * Use LongerPad.LEFT when centering.
     * Width of the display must be set to the assigned width or expression width, whichever is bigger.
     * If the operation is not valid, display "ERROR".
     *
     * @param a         number
     * @param b         number
     * @param name      full name of calculator company
     * @param operation operation ("addition" or "subtraction") applied to numbers a and b
     * @param width     width of the display
     * @return string representing the final format
     */

    private static String display(int a, int b, String name, String operation, int width) {

        return "";

    }


}