package day08;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AppTest {

    @Before
    public void setUp() throws Exception { // see tehakse enne jooksutamist
    }

    @After
    public void tearDown() throws Exception { // peale testi
    }

    @Test
    public void main() {
    }

    @Test
    public void testAddNumbers() {
        int result = App.addNumbers(2,2);
        assertTrue(result == 4);
        assertEquals(4, result);

    }
}