package day08;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataBasedEnCryptor extends Cryptor {


    public DataBasedEnCryptor(String filePath) throws IOException {
        super(filePath);

    }


    @Override
    public String translate(String text) {
        if (text != null) {
            String translatedText = "";
            char[] textChars = text.toCharArray();
            for (char c : textChars) {
                String letter = String.valueOf(c).toUpperCase();
                translatedText = translatedText + this.dictionary.get(letter);
            }
            return translatedText;
        }
        return null;
    }

    @Override
    protected Map<String, String> generateDictionary(List<String> fileLines) {
        Map<String, String> dictionary = new HashMap<>();
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DATE);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);
        for (String dictLine : fileLines) {
            String letter = dictLine;
            char c = dictLine.charAt(0);
            int d = (int) c;
            int encrypted = d + day + month + 1 + year;
            String message = Integer.toString(encrypted) +";";
            dictionary.put(letter, message);
        }
        return dictionary;


    }


}


