

package day08;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
public abstract class Cryptor {
    protected Map<String, String> dictionary = null;

    public Cryptor(String filePath) throws IOException {
        List<String> fileLines = readAlphabet(filePath);
        this.dictionary = generateDictionary(fileLines);
    }

    protected List<String> readAlphabet(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        return Files.readAllLines(path);
    }
    protected abstract Map<String, String> generateDictionary(List<String> fileLines);

    public abstract String translate(String text);
}