package day08;

public class Dog {

    private int age = 0;
    private int legCount = 4;
    private int tailLenght = 0;
    private String name = null;
    private String barkingInLanguage = null;


    // setter ja getter ALT + Insert
    public String getBarkingInLanguage() {
        return barkingInLanguage;
    }

    public void setBarkingInLanguage(String barkingInLanguage) {
        this.barkingInLanguage = barkingInLanguage;

    }

    public int getTailLenght() {
        return tailLenght;
    }

    public String getName() {
        return name;
    }

    public static int getCount() {
        return count;
    }

    public void setLegCount(int legCount) {
        this.legCount = legCount;
    }

    public int getLegCount() {
        return this.legCount;
    }

    public int getAge() {
        System.out.println("The dog " + this.name + " age was requested...");
        return this.age;
    }

    public void setAge(int dogAge) {
        if (dogAge > 0 && dogAge < 25) {
            System.out.println("The dog " + this.name + " age was changed from " + this.age + " to " + dogAge);

            this.age = dogAge;
        } else {
            System.out.println("Invalid age setting attempt to " + this.name);
        }

    }

    public void setName(String dogName) {

        this.name = dogName;
    }

    private static int count = 0; // See muutuja ei kuulu objektile (static). Sellest count muutujast ei tehta koopiat objektile.

    public Dog() {
        Dog.count++;
    }


    public void run() {
        if (age > 5) {
            System.out.println("Run slowly");
        } else {
            System.out.println("Run fast");
        }
    }

    public void bark(int barkTime) {
        for (int i = 0; i < barkTime; i++) {
            System.out.println("Auh..");
        }}


    public void printFellowCount() {
        System.out.println("My name is  " + this.name + " and I have " + Dog.count + " friends.");
    }

    public static void printDogCount() {
        System.out.println("You have created " + Dog.count + " dogs so far. Keep going.");
    }


    public static void printDogAge(Dog dog) {
        System.out.println("The dog age is " + dog.age);
    }




}
