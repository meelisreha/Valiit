package day10;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class VisitDay {

    private String date;
    private int visits;


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getVisits() {
        return visits;
    }

    public void setVisits(int visits) {
        this.visits = visits;
    }

    public VisitDay(String date, Integer visits) {
        this.date = date;
        this.visits = visits;
    }

    public static List<VisitDay> getVisitorsList() {
        return visitorsList;
    }

    public static void setVisitorsList(List<VisitDay> visitorsList) {
        VisitDay.visitorsList = visitorsList;
    }

    public static List<VisitDay> visitorsList = new ArrayList<>();


    public static void importVists(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        List<String> filesLines = Files.readAllLines(path);
        for (String fileLine : filesLines) {
            String[] lineParts = fileLine.split(", ");
            visitorsList.add(new VisitDay(lineParts[0], Integer.parseInt(lineParts[1])));
        }

    }

    public static void higestVisitorDay() {

        Collections.sort(visitorsList, new Comparator<VisitDay>() {

                    public int compare(VisitDay o1, VisitDay o2) {
                        return Integer.compare(o1.visits, o2.visits);
                    }
                }
        );
        Object highestValue = visitorsList.get(visitorsList.size()-1);
        Object lowestValue = visitorsList.get(0);
        System.out.println("Kõige rohkem külastajaid oli: ");
        System.out.println(((VisitDay) highestValue).getDate() );
        System.out.println("Külastajate arv oli: " +((VisitDay) highestValue).getVisits());


        System.out.println("Kõige vähem külastajaid oli: ");
        System.out.println(((VisitDay) lowestValue).getDate());
        System.out.println("Külastajate arv oli: " +((VisitDay) lowestValue).getVisits());

    }


    @Override
    public String toString() {

        return String.format("[%s, %s]", this.date, this.visits);
    }

    public static void sortedVisitorsList () {

        Collections.sort(visitorsList, new Comparator<VisitDay>() {

                    public int compare(VisitDay o1, VisitDay o2) {
                        return Integer.compare(o1.visits, o2.visits);
                    }
                }
        );
        Collections.reverse(visitorsList);
        for (int i = 0; i < visitorsList.size(); i++){
            System.out.println(visitorsList.get(i));
        }



    }





}




