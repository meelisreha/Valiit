package day10;

import java.io.IOException;


public class ExercisesDay10 {

    public static void main(String[] args) throws IOException {

        VisitDay.importVists(args[0]);
        VisitDay.higestVisitorDay();
        VisitDay.sortedVisitorsList();


    }
}
