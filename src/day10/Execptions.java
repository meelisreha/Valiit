package day10;

import java.util.Random;
import java.util.Scanner;

public class Execptions {
    public static void main(String[] args) {
        String name;
        String password = "-";
        Scanner sc = new Scanner(System.in);

        Random rn = new Random();
        int answer = rn.nextInt(100000) + 1;
        int answer1 = rn.nextInt(1000000) + 1;

        System.out.println("Tell me your name!");
        name = sc.nextLine();
        sc.close();

        try {
            password = answer + name.substring(0, 3).toLowerCase() + answer1 + name.substring(name.length() - 3).toUpperCase();
        } catch (StringIndexOutOfBoundsException e) {
            System.out.println("Name too short");
        }

        System.out.println("Username: " + name);
        System.out.println("Password: " + password);

    }
}
