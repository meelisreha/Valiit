package Bookshelf;

import java.util.HashMap;
import java.util.Map;

public class Person {

    private String Name;
    private double money;

    public Person(String Name, int money) {
        this.Name = Name;
        this.money = money;

    }
    public String getName() {
        return Name;
    }

    public double getMoney() {
        return money;
    }

    private static Map<String, Person> clients = new HashMap<>();
}
