package Bookshelf;

public class BookShelf {
    public static void main(String[] args) {

        Person person1 = new Person("Meelis", 555);
        System.out.println(person1.getName()+ " " + person1.getMoney());

        Book rehePapp = new Book("Rehepapp", "Andrus Kivirähk", 2000, 120);
        Book mobyDick = new Book("Moby Dick", "Helman Melville", 1851, 100);
        System.out.println(String.format(" %s , %s, %s, %s" , rehePapp.getAuthor(), rehePapp.getTitle(), rehePapp.getYearOfPublishing(), rehePapp.getPrice()));
        System.out.println(rehePapp.getTitle());


    }
}
