package Bank;

import java.io.IOException;
import java.util.Scanner;

public class BankApp {
    public static void main(String[] args) throws IOException {

        System.out.println("Welcome to myBank!");
        Scanner inputScanner = new Scanner(System.in);

        Account.importClients(args[0]);

        System.out.println("The bank has following customers: ");
        System.out.println(Account.getClients());

        boolean keepRunning = true;
        while (keepRunning == true) {
            System.out.println("How can we help you today?");
            // kuula sisendit
            String input = inputScanner.nextLine().toUpperCase();
            if (input.startsWith("TRANSFER")) {
                System.out.println("Starting money transfer... ");
                String[] inputParts = input.split(" ");
                Account.transfer(inputParts[1], inputParts[2], Integer.parseInt(inputParts[3]));
            } else if (input.startsWith("BALANCE")) {
                System.out.println("Displaying account balance ");
                String[] inputParts = input.split(" ");
                if (inputParts.length == 2) {
                    System.out.println("Searching by account number...");
                    Account.displayClientInfo(inputParts[1]);
                } else {
                    System.out.println("Searching by first name and last name...");
                    Account.displayClientInfo(inputParts[1], inputParts[2]);
                }
            }else if (input.startsWith("EXIT")){
                keepRunning = false;
                System.out.println("Exiting bank");
                Account.writeToFileOnExit();
            }

            else {
                System.out.println("Incorrect command. ");
            }
        }

    }
}
