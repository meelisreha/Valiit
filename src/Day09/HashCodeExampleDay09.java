package Day09;

public class HashCodeExampleDay09 {
    public static void main(String[] args) {


        Car fordSierra = new Ford();
        fordSierra.setColor("Valge");
        fordSierra.setTopSpeed(120);
        fordSierra.setModel("Sierra");


        Car fordFiesta = new Ford();
        fordFiesta.setColor("Valge");
        fordFiesta.setTopSpeed(120);
        fordFiesta.setModel("Sierra");


        Car fordFocus = new Ford();
        fordFocus.setColor("Must");
        fordFocus.setTopSpeed(120);
        fordFocus.setModel("Focus");


        Car fordKa = new Ford();
        fordKa.setColor("Sinine");
        fordKa.setTopSpeed(50);
        fordKa.setModel("Ka");


        System.out.println("Ford Sierra hashcode: " + fordSierra.hashCode());
        System.out.println("Ford Fiesta hashcode: " + fordFiesta.hashCode());
/*        fordFiesta = fordSierra; // objekt 1 = objekt2, fordFiesta objekt jääb heapi ja kustutakse varsti ära, mõlemad viited viitavad samale asjale nüüd
        fordFiesta.setColor("Red");*/
        System.out.println(fordSierra.getColor()); // kuna objekt1 = objekt 2 võtab esimene objekt teiselt üle omadused
        System.out.println(fordFiesta == fordSierra); // võrdleb mäluasukohta, mis on enamasti identne

        System.out.println("Objekti võrdlemine iseendaga: " + fordFiesta.equals(fordFiesta)); // objekt on alati võrdne iseendaga
        System.out.println("Objekti võrdlemine nulliga(mittemillegag): " + fordFiesta.equals(null));// Ei tohi olla null
        System.out.println("Auto obkeit võrdlemine string objektiga: " + fordFiesta.equals("Tere")); // classid peavad võrduma

        System.out.println("Objekti võrdlemine teise loogiliselt samaväärse objektiga(Ford - Fordiga): " + fordSierra.equals(fordFiesta)); // kas on Ford(Class)?


        System.out.println(fordFocus.compareTo(fordKa));
        System.out.println(fordKa.compareTo(fordFocus));
        System.out.println(fordKa.compareTo(fordKa));

        if (fordKa.compareTo(fordFocus) < 1) {
            //fordKa on aeglasem to kui ford Focus
        }
        if (fordKa.compareTo(fordFocus) > 1) {
            //fordKa on kiirem kui fordFocus
        }
        if (fordKa.compareTo(fordFocus) == 0) {
            //fordKa on samakiire kui fordFocus
        }


    }

}
