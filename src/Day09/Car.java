package Day09;

import java.util.Objects;

public abstract class Car implements BasicCar , Comparable<Car> {

    private String color = null;
    private int topSpeed = 0;
    private int price = 0;
    private String model = null;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(int topSpeed) {
        this.topSpeed = topSpeed;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }


    public abstract void drive();


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Car otherCar = (Car) obj; // deklareerin otherCari muutuju ja panen väärtuseks objekti ja deklareerin Car tüüpi
        boolean colorSame = this.getColor().equals(otherCar.getColor());
        boolean speedSame = this.getTopSpeed() == otherCar.getTopSpeed();
        boolean sameModel = this.getModel().equals(otherCar.getModel());
        return colorSame && speedSame && sameModel; // kõik booleanid peavad olema true, muidu on vastus false
    }

    @Override
    public int hashCode() { //kui equals meetod overrideda tuleb hashCode meetodi ka defineerima
        int colorHashCode = Objects.hashCode(this.getColor());
        int modelHashCode = Objects.hashCode(this.getModel());
        int topSpeedHashCode = this.getTopSpeed();
        return Math.abs(567 * colorHashCode * modelHashCode * topSpeedHashCode); // Hashcode võib korduda ning peab olema ka muu kontrollimisvariant
    }

    @Override
    public int compareTo(Car o) {
        return this.getTopSpeed() - o.getTopSpeed();
    }
}


