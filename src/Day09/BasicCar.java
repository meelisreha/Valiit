package Day09;

public interface BasicCar {

    void setColor(String color);
    String getColor();

    void setPrice(int price);
    int getPrice();

    void setModel(String mark);
    String getModel();

    void setTopSpeed(int topSpeed);
    int getTopSpeed();

    void drive();

}
