package day01;

import java.math.BigDecimal;

public class StringDay01andDay02 {

    public static void main(String[] args) {
/*      // MUUTUJAD - 2 päev
        // ARVUD
        byte b1 = 2;
        byte b2 = 3;
        byte b3 = (byte) (b1 + b2); // peab ütlema, et on byte.

        System.out.println(b3);

        float f1 = 1.1f;
        float f2 = 2.2f;
        float f3 = f1 + f2; // võib kasutada ka floati, kuid pole väga täpsed ja ei ole parimad varindid, kui on head kuni 5-6 kohta peale koma.
        System.out.println(f3);

        BigDecimal bd1 = new BigDecimal("24534263.0000000000000000000000000000000000"); // BigDecimal on suurte arvude hoidmiseks ja arvutamiseks, - peab import java.math.BigDecimal; tegema (Intellij annab ise)
        BigDecimal bd2 = new BigDecimal("2"); // võib ka kaustada int  - BigDecimal on aeglne ja vaja kaustada ainult siis kui on väga suured arvud
        BigDecimal bd3 = bd1.multiply(bd2); // multiply meetod
        System.out.println(bd3);

        boolean false1 = false; // kui booleanil väärtust pole on by default false
        boolean bo2 = true;
        boolean bo3 = false1 && bSo2;
        System.out.println(false1);
        System.out.println(bo2);
        System.out.println(bo3);

        char ch1 = 'a';
        char ch2 = 'b';
        char ch3 = '3';
        System.out.println((short) ch1);
        System.out.println((short) ch2);
        System.out.println((short) ch3);

        Integer i = 5;
        Integer i2 = new Integer(5);
        int j = 5;
        int i3 = i + i2;
        System.out.println(i3);

        int a = 4;
        boolean visitorsList = a == 4; // kontrollib kas a on 4
        String s1 = "Test1";
        String s2 = "3";
        String s3 = s1 + s2; // = Test13 liidab kaks erinevat sõnet kokku
        System.out.println(s3);

        int il1 = 2;
        int il2 = il1 + il1;
        System.out.println(i2);

        int i = 1;
        int i2 = i++;
        System.out.println(i2);
        i = i + 1; // i = 2 (+1)
        i++; // i = 3 (+1)
        i--; // i = 2 (-1)
        ++i;
        --i;

        int j1 = 10;
        int j2 = --j1;
        System.out.println(j1);
        System.out.println(j2);


        int u = 5;
        u = u + 8; // sama tehe mis selle all
        u += 8;

        u = u - 3; // sama tehe mis alumine
        u -= 3;



        int i5 = 8;
        System.out.println(i5);
        i5 = 3 * i5;

        i5 *= 3;
        System.out.println(i5);
        double d5 = i5 / 13d;
        System.out.println(d5);
        boolean b1 = true;
        boolean b2 = false;
        boolean b3 = !b2; // ! pöörab tõeväärtuse vastupidiseks
        boolean b4 = (b1 && b2) || b3;
        System.out.println(b4);

        if ((b1 && b2) || b3) { // && = and || = or
            // tee midagi
        } else {
            // tee midagi muud
        }
        int i = 10;
        int u = 11 % 3; // % on jäägi tagastmine ehk antud juhul on jääk 2
        System.out.println(u);

*/      // STRINGID

        String s1 = "text1";
        String s2 = new String("text1");
        String s3 = s1 + s2;
        s1 = s1 + s2;
        System.out.println(s3);
        System.out.println(s1);

        System.out.println("Esimene argument: " + args[0] + " ja Teine argument: " + args[1]);

        String t1 = args[0];
        String t2 = args[1];

        System.out.println(t1 == t2); // ei ole võimalik 2-te stringi omavahel niimoodi võrrelda, kuna on erinevad objektid kuigi sisu on sama
        System.out.println(args[0] == args[1]);

        t1 = t2;
        System.out.println(t1 == t2);

        String x1 = new String("Midagi1");
        String x2 = new String("midagi1");
        System.out.println(x1.equals(x2)); // Saab omavahel stringi sisu kontrollida - kontrollib ka väiksemid ja suuremaid tähti
        System.out.println(x1.equalsIgnoreCase(x2)); // Saab omavahel stringi sisu kontrollida - ei kontrolli suuri/väikseid tähti vaid sisu.

    }
}
