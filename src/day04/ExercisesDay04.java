package day04;


import sun.reflect.generics.tree.Tree;

import java.util.*;

public class ExercisesDay04 {
    public static void main(String[] args) {
/*

//        Ülesanne 11
        int i = 1;
        while (i <= 100) {
            System.out.println(i);
            i++;
        }

//        Ülesanne 12
        for (i = 1; i <= 100; i++) {
            System.out.println(i);
        }

//        Ülesanne 13

        int[] arr1 = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int x : arr1) {
            System.out.println(x);
        }

//        Ülesanne 14

        for (i = 1; i <= 100; i++) {
            if (i % 3 == 0) {
                System.out.println(i);
            }
        }

//        Ülesanne 15
        String[] arr2 = {"Sun", "Metsatöll", "Queen", "Metallica"};
        for (i = 0; i < arr2.length; i++) {
            System.out.print(arr2[i]);
            if (i < arr2.length - 1) {
                System.out.print(", ");
            }
        }

//        Ülesanne 16
        String[] arr3 = {"Sun", "Metsatöll", "Queen", "Metallica"};
        for (int u = arr3.length - 1; u >= 0; u--) {
            System.out.print(arr2[u]);
            if (u > 0) {
                System.out.print(", ");
            }
        }

//        Ülesanne 17
        for (String numString : args) {
            int num = Integer.parseInt(numString);
            switch ([j][3]num) {
                case 0:
                    System.out.println("Null");
                    break;
                case 1:
                    System.out.println("Üks");
                    break;
                case 2:
                    System.out.println("Kaks");
                    break;
                case 3:
                    System.out.println("Kolm");
                    break;
                case 4:
                    System.out.println("Neli");
                    break;
                case 5:
                    System.out.println("Viis");
                    break;
                case 6:
                    System.out.println("Kuus");
                    break;
                case 7:
                    System.out.println("Seitse");
                    break;
                case 8:
                    System.out.println("Kaheksa");
                    break;
                case 9:
                    System.out.println("Üheksa");
                    break;
                case 10:
                    System.out.println("Kümme");
                    break;

            }

//            Ülesanne 18
            double s;
            do {
                System.out.println("Tere");
                s = Math.random();
            } while (s < 0.5d);

        }

//        Ülesanne 19

        List<String> cities = new ArrayList<>();
        cities.add("Tartu");
        cities.add("Tallinn");
        cities.add("Võru");
        cities.add("Põlva");
        cities.add("Jõgeva");
        System.out.println(cities.get(0));
        System.out.println(cities.get(2));
        System.out.println(cities.get(cities.size() - 1));
        System.out.println(cities.get(1));
        System.out.println(String.format("Esimene: %s, Teine: %s, Kolmas: %s", cities.get(0), cities.get(1), cities.get(2)));

//        Ülesanne 20
        LinkedList<String> queue1 = new LinkedList<>();
        queue1.add("Üks");
        queue1.add("Kaks");
        queue1.add("Kolm");
        queue1.add("Neli");
        queue1.add("Viis");
        queue1.add("Kuus");
        queue1.add("seitse");
        queue1.push("Null");
        queue1.removeIf(x -> x.equals("Neli"));
        queue1.removeIf(x -> x.toLowerCase() == x);
        System.out.println(queue1.getFirst());
        while (!queue1.isEmpty()) {
            System.out.println(queue1.remove());
        }
        ;
        System.out.println(queue1);

//        Ülesanne 21
        Set<String> treeset1 = new TreeSet<>();
        treeset1.addAll(Arrays.asList("Marjad", "Munad", "Saiad", "Pähklid", "Nätsud", "12345134141"));
        treeset1.removeIf(x -> x.startsWith("1"));
        treeset1.forEach((x -> System.out.println(x)));

//        Ülesanne 22
        Map<String, List<String>> map1 = new HashMap<>();
        map1.put("Estonia", Arrays.asList("Tallinn", "Tartu", "Võru", "Valga"));
        map1.put("Sweden", Arrays.asList("Stockholm", "Uppsala", "Lund", "Köping"));
        map1.put("Finland", Arrays.asList("Helsinki", "Espoo", "Hankoo", "Jämsa"));
        // Esimene viis
        for (String key : map1.keySet()) {
            System.out.println("Country " + key + "\n Cities: ");
            for (String city : map1.get(key)) {
                System.out.println(" " + city + " ");
            }
            System.out.println();

        }

        // Teine viis
        map1.forEach((countryName, cityNames) -> {
            System.out.println("Country " + countryName + "\nCities: ");
            for (String city : cityNames) {
                System.out.println(" " + city + " ");
            }
*/
//      For

        for (
                int z = 0; z < 10_000_000; z = z + 3)
            ;

//      For Each
        for (
                int y : new int[]{5, 6, 8, 9 })
        {
            System.out.println(y);
        }
//      While
        boolean shouldContinue = true;
        while (shouldContinue)

        {
            if (Math.random() > 0.5d) {
                System.out.println("Lase edasi");
            } else {
                System.out.println("Aitab");
                shouldContinue = false;
            }

        }
//      Do While
        shouldContinue = true;
        do {
            System.out.println("Jätkan ketramist...");
            if (Math.random() > 0.2d) {
                System.out.println("Lõpetan");
                shouldContinue = false;
            }
        } while (shouldContinue);

//        Kollektsioonid
        List<String> list1 = new ArrayList<>(); // nimekiri elmentidest

        Set<String> set1 = new HashSet<>(); // nimekiri unikaalsetest elementidest

        Map<String, String> map1 = new HashMap<>(); // võti - väärtus paaride nimekiri
//    Kollektsioon vs massiiv (array):
//    Masiivil kindel pikkus, kollektsiooni pikkus võib muutuda.
//    Kollektsioonil palju abimeetodeid, massiivil need puuduvad.
//    Masiiv ei oska enda sisu välja printida, kollektsioon oskab.
//    Masiiv raiskab vähem resursse, kollektsioon on kallis (mälu, protsessori jõudlus).

    }

}






