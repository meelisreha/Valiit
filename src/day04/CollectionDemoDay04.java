package day04;

import java.util.*;

public class CollectionDemoDay04 {
    public static void main(String[] args) {
        int[] arr1 = {4, 5, 6, 7, 12, 8, 9, 11};
        System.out.println(arr1[2]);
        arr1[2] = 97;
        System.out.println(arr1[2]);
        System.out.println(arr1.length);


        int u = 10 - arr1.length;
        List<Integer> list1 = new ArrayList<>(Arrays.asList(5, 7, 9, 2, 3, 5, 8, 2));
        list1.add(99); // by default lõppu
        list1.add(3, 1000); // index , element
        list1.remove(2); //indexiga
        list1.removeIf((x) -> {
            return x == u;
        }); // kindla elemendi eemaldamine
        int size = list1.size();
        list1.forEach(
                x -> {
                    System.out.println("Prindin välja väärtused: ");
                    System.out.println("Elemendi väärtus on " + x);
                    System.out.println("Elemendi väärtuse kasv on " + (x + 1));
                    System.out.println("--------");

                }
        );
        for (Integer x : list1) {
            System.out.println("Prindin välja väärtused: ");
            System.out.println("Elemendi väärtus on " + x);
            System.out.println("Elemendi väärtuse kasv on " + (x + 1));
            System.out.println("--------");
        }
        System.out.println(list1.size()); // collectionite puhul on lenghti asemel size
        System.out.println(list1.get(3));
        System.out.println(list1);

        Set<String> set1 = new HashSet<>(Arrays.asList("Tartu", "Tallinn", "Kollane")); // talletab suvalise järjekorda - juhuslik (Hashi järgi)
        set1.add("Mario");
        set1.add("Vannilla");
        set1.add("Tartu"); // ei lisa kuna hoiab unikaalseid väärtusi
        String[] elementsFromSet = set1.toArray(new String[0]);
        set1.remove("Vannilla"); // eemaldame listist
        set1.removeIf(x -> x.equals("Tallinn")); // eemaldame konkreetse elemendi mingi kindla parameetri järgi
        System.out.println(set1);
        System.out.println(elementsFromSet[2]);


        Set<String> tset1 = new TreeSet<>(Arrays.asList("Tartu", "Tallinn", "Kollane")); // tähestikulises järjekorras
        tset1.add("Mango");
        tset1.add("Tartu"); // ei lisa kuni ainult unikaalseid sõnu saab hoida
        tset1.removeIf(x -> x.length() == 5); // kindla pikkusega Stringi kustutamine
        System.out.println(tset1);

        Set<String> set2 = new TreeSet<>(new Comparator<String>() { // Et panna loogilise järjekorda
            @Override
            public int compare(String o1, String o2) {
                return o2.compareTo(o1);
            }
        });
        set2.addAll(Arrays.asList("Tartu", "Tallinn", "Kollane", "samba", "Paris", "Punane", "keys", "Leelo"));
        set2.add("Munamägi");
        set2.removeIf(x-> x == x.toLowerCase()); // eemaldab lowercase
        set2.removeIf(x-> x.startsWith("P")); // Kõik P-ga algavad sõnad võtab ära
        System.out.println(set2);



        Map<String, String> map1 = new HashMap<>(); // Map<Key, Value> nimi = ....
        map1.put("Parool", "Pass123"); // addi asemel on put käsklus
        map1.put("Kasutajanimi" , "Kollane");
        map1.put("Kood", "1234");
        map1.put("Nimi", "Meelis");
        map1.put("Email", "mis@hot.ee");
        map1.remove("Kood");
        map1.remove("Parool","Pass124");
        System.out.println(map1);
        System.out.println(map1.get("Nimi")); // key vastus
        System.out.println(map1.keySet());
        System.out.println(map1.values());
        System.out.println(map1.containsValue("Meelis"));
        map1.forEach((a, b)->{
            System.out.println(a = a + " key");
            System.out.println(b = b + " Value");
        });
        for (String key : map1.keySet()){
            System.out.println("Key: " + key + " = " + map1.get(key));
        }

        Map<Object, Object> obj1 = new HashMap<>(); // Saab arrayd kasutada
        obj1.put("Kasutajanimi", "Meelis");
        obj1.put("Info", Arrays.asList("Tark", "Eestlane", "Suured kõrvad", "178cm pikk"));
        System.out.println(obj1);

    }
}
