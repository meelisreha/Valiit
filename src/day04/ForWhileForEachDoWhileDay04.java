package day04;

public class ForWhileForEachDoWhileDay04 {
    public static void main(String[] args) {

//      FOR (int i = 0 Tsükli muutuja algväärtustamine; tingimus; tsükli muutuja väärtuse muutmine peale igat tsüklit ){tee midagi;} - klassikaline
        int[] arr1 = {11, 29, 13, 44, 95};
        for (int i = 0; i < arr1.length; i++) {  // i = i + 1
            System.out.println("TERE " + arr1[i]);
        }

        for (int i = arr1.length - 1; i >= 0; i--) { // vastupidine array läbi jooksutamine, tagurpidi - (tsükli algväärtus on array pikkus -1 (hakkab 0,1,2,3,4); i peab olema võrdne või suurem kui 0, et töötaks; i = i - 1 iga tsükliga)
            System.out.println("TERE " + arr1[i]);

        }

//      WHILE (tõeväärtus){käsklus;}


//       loetleb arr1 sisu
        int u = 0;
        while (u < arr1.length) { // annab väärutse u = 0 ja kuni u on väiksel masiivi pikkusega teeb midagi
            System.out.println("TERE " + arr1[u++]);
        }
        //tagant poolt ettepooole
        int a = arr1.length - 1; // a on masiivi pikkus -1 (kuna masiiv hakkab 0st ja on 5 pikkuseks mitte 4 nagu on masiivis kohad) ja kuni a on suurem või võrdne 0-ga pridinb välja masiivi vastava koha väärtuse.
        while (a >= 0) {
            System.out.println("TERE " + arr1[a--]);
        }


//        for(;;); - igavene tsükel
//        while(true); - igavene tsükel


//      FOREACH tsükel for (element x : array sees){ - Tee midagi iga elemendiga; }
        for (int x : arr1) {
            System.out.println("TERE " + x);
        }

//      DOWHILE - Peab kindel olema, et masiivil on vähemalt 1 element! do {tee midagi; } while (tõeväärtus = false);

        int y = 0;
        do {
            System.out.println("TERE " + arr1[y++]);
        } while (y > arr1.length);

//        BREAK && CONTINUE - break; - katkestab tsükli tegevuse, continue; - jätkame uue tsükliga

        for (int i = 1; i < 10000; i++) {
            if (i == 649) {
                System.out.println("Lõpetame selle tsükli tegevuse");
                continue; // jätkame uue tsükliga
            }
            if (i % 650 == 0) {
                System.out.println(i);
                break; // lõpetame tsükli tegevuse
            }

        }
        // kole ja ebakorrektne lahendus!
        someLabel:
        for (int i = 0; i < 10000; i++) {
            for (int s = 456; s < 634; s++) {
                if (s == 500) {
                    break someLabel;
                }
            }


        }

        someFunctionWithTwoLoops();

    }
//  Viisakas lahendus
    public static void someFunctionWithTwoLoops() {
        for (int i = 0; i < 10000; i++) {
            for (int s = 456; s < 634; s++) {
                if (s == 500) {
                    return;
                }
            }
        }
    }
}