package day04;

import java.util.*;

public class GameForNerdsDay04 {
    static Map<String, Integer> scoreBoard = new HashMap<>();
    public static void main(String[] args) {
        Scanner playerNumberIn = new Scanner(System.in); // võtab kasutaja numbri
        System.out.println("Paku number vahemikus 1 - 10000.");
        String playerDecision = null; // annab null väärtuse mängija otsusele
        int score = 0;
        do {
            System.out.println("Sisesta nimi: ");
            String playerName = playerNumberIn.next();
            score = gameForNerds(playerNumberIn);// jookutab mängu funktsiooni
            scoreBoard(playerName, score);
            System.out.println(scoreBoard);
            System.out.println("Kas soovid mängida uuest? y/n"); // kui kasutaja ära arvab, pakkub uuesti mängida
            playerDecision = playerNumberIn.next(); // loeb kasutaja vastus
        } while (playerDecision.equals("y")); // kui kasutaja vastus on y
        System.out.println("Ära proovi rohkem, Sul kulus liiga kaua aega!");



    }

    public static int gameForNerds(Scanner playerNumberIn) { // mängu meetod
        int counter = 1; // mängude arv 1
        int playerNumber = 0; // mängida number
        int gameNumber = new Random().nextInt(10000) + 1; // võtab 1 - 10000 numbri seast randomi ja paneb arvutile
        do {
            System.out.println("Sinu pakkumine: ");
            playerNumber = playerNumberIn.nextInt();
            System.out.println(gameNumber); //- Näitab mängu numbrit.
            if (playerNumber == gameNumber) {
                System.out.println("Õige, sul kulus " + counter + " korda, et ära arvata");
            } else if (playerNumber < gameNumber) {
                System.out.println("Liiga väike.");
                counter++; // lisab counterile iga pakkumise
            } else {
                System.out.println("Liiga Suur.");
                counter++;
            }
        } while (playerNumber != gameNumber); // nii kaua kui ei mängija ja mängu number ei klapi teeb do-d
    return counter;
    }

    public static void scoreBoard (String playerName, Integer score) {
        scoreBoard.put(playerName, score);


    }
}