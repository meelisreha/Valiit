package day06;

public class ExercsisesRepeatingDay06 {
    public static void main(String[] args) {
        String number = "1234567";
        System.out.println("Number: " + number);
        char[] digits = number.toCharArray(); // teen charride massiivi
        String reversedNumbers = "";
        for (int i = 0; i < digits.length; i++) {
            reversedNumbers = digits[i] + reversedNumbers;
        }
        System.out.println("Reversed number: " + reversedNumbers);
    }
}
