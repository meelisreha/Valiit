package day06;

import java.lang.invoke.SwitchPoint;

public class Person {


    private String personCode;

    public Person(String personCode) {
        this.personCode = personCode;

    }

    public int getBirthYear() {
        if (personCode != null) {
            switch (personCode.substring(0, 1)) {
                case "1":
                case "2":
                    return Integer.parseInt("18" + personCode.substring(1, 3));
                case "3":
                case "4":
                    return Integer.parseInt("19" + personCode.substring(1, 3));
                case "5":
                case "6":
                    return Integer.parseInt("20" + personCode.substring(1, 3));
                case "7":
                case "8":
                    return Integer.parseInt("21" + personCode.substring(1, 3));
            }
        }
        return 0;
    }

    public String getBirthMonth() {
        if (personCode != null && personCode.length() == 11) {
            String birthMonthNumber = personCode.substring(3, 5);
            switch (birthMonthNumber) {
                case "01":
                    return "Jaanuar";
                case "02":
                    return "Veebruar";
                case "03":
                    return "Märts";
                case "04":
                    return "Aprill";
                case "05":
                    return "Mai";
                case "06":
                    return "Juuni";
                case "07":
                    return "Juuli:";
                case "08":
                    return "August";
                case "09":
                    return "September";
                case "10":
                    return "Oktoober";
                case "11":
                    return "November";
                case "12":
                    return "Detsember";
            }

        }
        return "Teadmata";
    }

    public int getDayOfMonth() {
        if (personCode != null && personCode.length() == 11) {
            return Integer.parseInt(personCode.substring(5, 7));
        }
        return 0;
    }

    public Gender getGender() {
        if (personCode != null && personCode.length() == 11) {
            int genderDeterminating = Integer.parseInt(personCode.substring(0, 1));
            if (genderDeterminating % 2 == 0) {
                return Gender.FEMALE;
            } else {
                return Gender.MALE;
            }
        }
        return Gender.UNKNOWN;
    }
}


