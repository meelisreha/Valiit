package day06;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CountryInfo {

    public String countryName;
    public String primeMinister;
    public String capital;
    public List<String> languages = new ArrayList<>();

    public CountryInfo(String countryName, String capital, String primeMinister, Collection<String> languages) {
        this.countryName = countryName;
        this.capital = capital;
        this.primeMinister = primeMinister;
        this.languages.addAll(languages);
    }

    ;

    @Override
    public String toString() {
        String textOutput = this.countryName;
        textOutput += "\n";
        for (String language : this.languages) {
            textOutput += "      " + language + "\n";

        }
        return textOutput;
    }


}
