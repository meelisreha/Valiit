package day06;

import java.util.Scanner;

public class OoBasicsDay06 {
    public static void main(String[] args) {

//        OoBasicsDay06 OoBasicsDay06 = new OoBasicsDay06();

        Dog myCooldDogName = new Dog();
//        System.out.println(myCooldDogName.legCount); . Ei saa kuna dog legcount on private
        myCooldDogName.getLegCount();
//        myCooldDogName.name = "Muki";
        myCooldDogName.setName("Muki");
//        myCooldDogName.age = 5;
        myCooldDogName.setAge(7);
        myCooldDogName.getAge();
        myCooldDogName.getTailLenght();
        myCooldDogName.bark(4);
        myCooldDogName.run();


        Dog dog2 = new Dog();
        Dog dog3 = new Dog();
        dog3.setName("Rex");
        Dog notSoAwesomeDog = new Dog();
        notSoAwesomeDog.setName("Tipa");


        Dog.printDogCount();
        Dog.printDogAge(myCooldDogName);
        dog2.printFellowCount();

        System.out.println("PRINTING THE TOTAL DOG COUNT: " + Dog.getCount());


        Scanner scanner = new Scanner(System.in);

        Person person1 = new Person("38905070261");
        System.out.println(person1.getBirthYear());
        System.out.println(person1.getBirthMonth());
        System.out.println(person1.getDayOfMonth());
        if (person1.getGender()==Gender.MALE){
            System.out.println("This person is male");
        } else if(person1.getGender()== Gender.FEMALE) {
            System.out.println("This person is female");
        } else {
            System.out.println("This person gender is UNKNOWN");
        }


    }
}
