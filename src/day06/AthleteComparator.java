package day06;

import java.util.Comparator;

public class AthleteComparator implements Comparator<Athlete> {

    @Override
    public int compare(Athlete o1, Athlete o2) {

/*        if (o1.getAge() < o2.getAge()) {
            return -1;

        }else if (o1.getAge() > o2.getAge()){
            return 1;
        }
        return 0;*/
        int result = o1.getLastName().compareTo(o2.getLastName());

        if (result == 0) {
            result = o1.getFirstName().compareTo(o2.getFirstName());
            if (result == 0) {
                result = o1.getAge() - o2.getAge();
                if (result == 0) {
                    double weigthDIff = o1.getWeight() - o2.getWeight();
                    if (weigthDIff > 0.0d){
                        result = 1;
                    }else if(weigthDIff < 0.0d) {
                        result = -1;
                    }else {
                        result = 0;
                    }
                }
            }
        }
        return result;


        // Kui o1 on väiksem, kui o2  ---> tagasta negatiivne number
        // Kui o1 on suurem, kui o2 ----> tagasta positiivne number
        // Kui o1 ja o2 on võrdsed ---> tagasta 0

    }
}
