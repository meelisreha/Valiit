package day06;

import java.math.BigInteger;

public class ExercsisesDay06 {
    public static void main(String[] args) {
/*
        boolean isTrue = visitorsList(1);
        if (visitorsList(1)) {
            System.out.println("Test = ");
        }

        test2("Kollane", "Sinine");
        visitorsList(56);
        double addVat = addVAT(100);
        System.out.println(addVat);

        int[] exercise4Arr = exercise4(3, 4, true);
        for (int n : exercise4Arr) {
            System.out.print(n + " ");
        }

        String gender = deriveGender("68905070261");
        System.out.println(gender);
        printHello();
        System.out.println(deriveBirthDay("38905070261"));
        BigInteger proov = new BigInteger("37605030299");
        System.out.println(validatePersonalCode(proov));
*/
/*        for (int i = 100; i > 0; i--) {
            System.out.println(i);
        }


        printOutNumbers(1000l);

    }

    static void printOutNumbers(long number) { // rekursiivsne funktsioon - Rekursioon
//        System.out.println(number);
        number = number - 1;
        if (number > 0) {
            printOutNumbers(number);
        }

    }

    public static boolean visitorsList(int testNumber) {
        if (testNumber < 5) {
            return true;
        } else {
            return false;
        }
    }

    static void test2(String p1, String p2) {

    }

    static double addVAT(double vat) {
        double priceWithVat = 1.2d * vat;
        return priceWithVat;
    }

    static int[] exercise4(int p1, int p2, boolean p3) {
//        return null; // saab ainult siis tagastada, kui tagastustüüp on objekt
        if (p3) { // p3 = true
            return new int[]{p1 * p1, p2 * p2};
        } else {
            return new int[]{0, 0};
        }

    }*/
    }


    static String deriveGender(String isikuKood) {
        if (isikuKood != null) {
            String gendernum = isikuKood.substring(0, 1); // 0 - 1 ja 1 väljaarvatud
            int gender = Integer.parseInt(gendernum);
            if (gender % 2 == 0) {
                return "F";
            } else {
                return "M";

            }
        }
        return null;
    }

    static void printHello() {
        System.out.println("Tere");
    }

    static int deriveBirthDay(String isikuKood) {
        if (isikuKood != null) {
            switch (isikuKood.substring(0, 1)) {
                case "1":
                case "2":
                    return Integer.parseInt("18" + isikuKood.substring(1, 3));
                case "3":
                case "4":
                    return Integer.parseInt("19" + isikuKood.substring(1, 3));
                case "5":
                case "6":
                    return Integer.parseInt("20" + isikuKood.substring(1, 3));
                case "7":
                case "8":
                    return Integer.parseInt("21" + isikuKood.substring(1, 3));
            }
        }
        return 0;
    }

    static boolean validatePersonalCode(BigInteger isikuKood) {
        if (isikuKood != null) {
            String isikuKoodString = isikuKood.toString();
            if (isikuKoodString.length() == 11) {
                int[] weights1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};

                int tmpSUm = 0;
                for (int i = 0; i < weights1.length; i++) {
                    int digit = Integer.parseInt(isikuKoodString.substring(i, i + 1));
                    tmpSUm += digit * weights1[i];

                }
                int checkNumber = tmpSUm % 11;
                if (checkNumber != 10) {
                    int personalCodeCheckNum = Integer.parseInt(isikuKoodString.substring(10, 11));
                    return personalCodeCheckNum == checkNumber;
                } else {
                    tmpSUm = 0;
                    int[] weigths2 = {3, 4, 5, 6, 7, 8, 9, 1, 2, 3};
                    for (int i = 0; i < weigths2.length; i++) {
                        int digit = Integer.parseInt(isikuKoodString.substring(i, i + 1));
                        tmpSUm += digit * weigths2[i];
                    }
                    checkNumber = tmpSUm % 11;
                    checkNumber = (checkNumber == 10) ? 0 : checkNumber;
                    int personalCodeCheckNum = Integer.parseInt(isikuKoodString.substring(10, 11));
                    return personalCodeCheckNum == checkNumber;
                }
            }

        }
        return false;


    }

    public static boolean isPersoanlCodeCorrect(String personalCode) {
        boolean result = validatePersonalCode(new BigInteger(personalCode));
        if (result) {
            int month = Integer.parseInt(personalCode.substring(3, 5));
            result = month > 0 && month < 13;
        }
        int month = Integer.parseInt(personalCode.substring(3, 5));
        int day = Integer.parseInt(personalCode.substring(5, 7));
        if (result) {
            if (month == 2) {
                result = day > 0 && day <= 29;
            } else {
                switch (month) {
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12:
                        result = day > 0 && day < 32;
                        break;
                    default:
                        result = day > 0 && day < 31;

                }
            }

            if (result) {
                int firstDigit = Integer.parseInt(personalCode.substring(0,1));
                result = firstDigit > 0 && firstDigit <7;
            }

        }
        return result;
    }


    /*      CountryInfo estonia = new CountryInfo("Estonia", "Tallinn", "Jüri Ratas", Arrays.asList("Estonian", "Latvian", "Russian", "Finnish"));
     *//*   estonia.countryName = "Estonia";
        estonia.capital = "Tallinn";
        estonia.primeMinister = "Jüri Ratas";
        estonia.languages.addAll(Arrays.asList("Estonian" , "Latvian", "Finnish", "Russian"));*//*


        CountryInfo latvia = new CountryInfo("Latvia", "Riga", "Maris Kucinskis", Arrays.asList("Latvian", "Ukrainain", "Russian"));
        *//*      latvia.countryName = "Latvia";
        latvia.capital = "Riga";
        latvia.primeMinister = "Maris Kucinskis";
        latvia.languages.addAll(Arrays.asList("Latvian", "Ukraina"));
        latvia.languages.add("Finnish");*//*

        System.out.println(estonia.capital);
        for(String language : estonia.languages){
            System.out.println(" " + language);
        }

        List<CountryInfo> countries = new ArrayList<>();
        countries.add(estonia);
        countries.add(latvia);

        for (CountryInfo country : countries) {
            System.out.println(country);
        }
*/
/*        Runner myRunner = new Runner();
        SkyDiver mySkyDiver = new SkyDiver();
        Athlete myGenericAthlethe = new Athlete() {
        };

        List<Athlete> athletes = new ArrayList<>();
        athletes.add(myRunner);
        athletes.add(mySkyDiver);
        athletes.add(myGenericAthlethe);




        for (Athlete athlete : athletes) {
//            athlete.perform();*/
//        }

}






