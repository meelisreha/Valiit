package day06;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class AthleteExampleExercise {

    public static void main(String[] args) {
        Runner myRunner1 = new Runner();
        myRunner1.setLastName("Kuusepuu");
        myRunner1.setFirstName("Johannes");
        myRunner1.setAge(99);
        myRunner1.setGender("M");
        myRunner1.setWeight(120.04d);

        Runner myRunner2 = new Runner();
        myRunner2.setLastName("Tamm");
        myRunner2.setFirstName("Toomas");
        myRunner2.setAge(12);
        myRunner2.setGender("M");
        myRunner2.setWeight(41.04d);

        Runner myRunner3 = new Runner();
        myRunner3.setLastName("Münt");
        myRunner3.setFirstName("Maris");
        myRunner3.setAge(65);
        myRunner3.setGender("F");
        myRunner3.setWeight(71.04d);

        Runner myRunner5 = new Runner();
        myRunner5.setLastName("Münt");
        myRunner5.setFirstName("Andrus");
        myRunner5.setAge(65);
        myRunner5.setGender("F");
        myRunner5.setWeight(71.04d);

        Runner myRunner4 = new Runner();
        myRunner4.setLastName("Aevastaja");
        myRunner4.setFirstName("Mart");
        myRunner4.setAge(85);
        myRunner4.setGender("M");
        myRunner4.setWeight(86.04d);

        SkyDiver mySkidiver1 = new SkyDiver();
        mySkidiver1.setFirstName("Kaidi-Liis");
        mySkidiver1.setLastName("Liim");
        mySkidiver1.setAge(28);
        mySkidiver1.setGender("F");
        mySkidiver1.setWeight(50.01d);

        SkyDiver mySkidiver2 = new SkyDiver();
        mySkidiver2.setFirstName("Leo");
        mySkidiver2.setLastName("Lendur");
        mySkidiver2.setAge(66);
        mySkidiver2.setGender("M");
        mySkidiver2.setWeight(70.5d);

        SkyDiver mySkidiver3 = new SkyDiver();
        mySkidiver3.setFirstName("Leo");
        mySkidiver3.setLastName("Lendur");
        mySkidiver3.setAge(31);
        mySkidiver3.setGender("M");
        mySkidiver3.setWeight(70.4d);

        List<Athlete> athletes = new ArrayList<>();
        athletes.add(myRunner1);
        athletes.add(myRunner2);
        athletes.add(myRunner3);
        athletes.add(myRunner4);
        athletes.add(myRunner5);
        athletes.add(mySkidiver1);
        athletes.add(mySkidiver2);
        athletes.add(mySkidiver3);

        athletes.sort(new AthleteComparator());

/*        athletes.sort(new Comparator<Athlete>() {
            @Override
            public int compare(Athlete o1, Athlete o2) {

*//*        if (o1.getAge() < o2.getAge()) {
            return -1;

        }else if (o1.getAge() > o2.getAge()){
            return 1;
        }
        return 0;*//*
                int result = o1.getLastName().compareTo(o2.getLastName());

                if (result == 0) {
                    result = o1.getFirstName().compareTo(o2.getFirstName());
                    if (result == 0) {
                        result = o1.getAge() - o2.getAge();
                        if (result == 0) {
                            double weigthDIff = o1.getWeight() - o2.getWeight();
                            if (weigthDIff > 0.0d) {
                                result = 1;
                            } else if (weigthDIff < 0.0d) {
                                result = -1;
                            } else {
                                result = 0;
                            }
                        }
                    }
                }
                return result;
            }
        });*/

        System.out.println(athletes);


    }
}
