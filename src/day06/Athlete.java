package day06;

import java.util.ArrayList;
import java.util.List;

public abstract class Athlete {

    protected String firstName;
    protected String lastName;
    protected int age;
    protected String gender;
    protected int length;
    protected double weight;


//    public List<String> omadused = new ArrayList<>();

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    protected void prepareForPerform() {
        // Valmistame sportlase ette sportimiseks
    }

    @Override
    public String toString() {
        return String.format("\n[fn=%S, ln=%S, a=%s, g=%s, w=%S]", this.getFirstName(), this.getLastName(), this.getAge(), this.getGender(), this.getWeight());

    }

    public abstract void perform();

    //        System.out.println("Default implementation. Please implement me in subclass. ");
}

