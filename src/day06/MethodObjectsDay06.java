package day06;

public class MethodObjectsDay06 {
    public static void main (String[] args) {
        int i = 5;
        double pi = 3.1419d * 3.1419d;
        double pi2 = Math.PI;
        double pi3 = Math.pow(getPi() ,2); // pi ruut
        int numberPow2 = doSomething(i);

    }

    public static double getPi() { // double annab väärtuse fromaadi, mis ta tagastab
        return 3.14159d;
    }


    public static int doSomething(int someNumber) {
        return someNumber * someNumber;
    }





}
