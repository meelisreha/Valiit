package day06;

import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.*;

public class ExercsisesDay06Test {

    @Test
    public void main() {
    }

    @Test
    public void testDeriveGender() {
        assertTrue(day06.ExercsisesDay06.deriveGender("381").equals("M"));
        assertTrue(day06.ExercsisesDay06.deriveGender("481").equals("F"));

    }


    @Test
    public void testValidatePersonalCode() {
        boolean result = day06.ExercsisesDay06.validatePersonalCode((new BigInteger("38905070261")));
        assertTrue(result);

        result = day06.ExercsisesDay06.validatePersonalCode(null);
        assertFalse(result);

        result = day06.ExercsisesDay06.validatePersonalCode(new BigInteger("1234"));
        assertFalse(result);

        result = day06.ExercsisesDay06.validatePersonalCode(new BigInteger("38104242728"));
        assertFalse(result);


    }
    @Test
    public void testisPersoanlCodeCorrect() {
        boolean result = day06.ExercsisesDay06.isPersoanlCodeCorrect("38905070261");
        assertTrue(result);

        result = day06.ExercsisesDay06.isPersoanlCodeCorrect("38944070261");
        assertFalse(result);

        result = day06.ExercsisesDay06.isPersoanlCodeCorrect("38905720261"); // kontrollib, et kui
        assertFalse(result);

        result = day06.ExercsisesDay06.isPersoanlCodeCorrect("38905070261");
        assertTrue(result);

    }


}