package day14;

public class CoffeeMachine {

    private int beanCount;
    private int waterAmount;
    private int waterAmountForCoffee = 300;
    private int beansForCoffee = 100;
    private int milk;
    private int milkForCappuccino = 200;
    private int missingMilk = milk - milkForCappuccino;
    private int missingWater = waterAmount - waterAmountForCoffee;
    private int missingBeans = beanCount - beansForCoffee;

    public int getMilk() {
        return milk;
    }

    public void setMilk(int milk) {
        this.milk = milk;
    }


    public int getBeanCount() {
        return beanCount;
    }

    public void setBeanCount(int beanCount) {
        this.beanCount = beanCount;
    }

    public int getWaterAmount() {
        return waterAmount;
    }

    public void setWaterAmount(int waterAmount) {
        this.waterAmount = waterAmount;
    }

    public void makeCoffee(int amount) {
        for (int i = 0; i < amount; i++) {
            if (beanCount >= beansForCoffee && waterAmount >= waterAmountForCoffee) {
                System.out.println("Kohvi on valmis");
                beanCount = beanCount - beansForCoffee;
                waterAmount = waterAmount - waterAmountForCoffee;
            } else if (beanCount < beansForCoffee) {
                System.out.println("Ei ole piisavalt kohviube, lisa kohviube!");
            } else if (waterAmount < waterAmountForCoffee) {
                System.out.println("Ei ole piisavalt vett, lisa vett!");
            }
        }
    }


    public void addBeansAndWater(int beans, int water) {
        this.waterAmount = water;
        this.beanCount = beans;
    }

    public void checkStatus() {
        System.out.println("Kohvimasinas on: " + getBeanCount() + " uba. ");
        System.out.println("Kohvimasins on: " + getWaterAmount() + " ml vett.");
    }

    public void makeHeavyCoffee(int amount) throws InterruptedException {
        for (int i = 0; i < amount; i++) {
            if (beanCount >= beansForCoffee * 2 && waterAmount >= waterAmountForCoffee) {
                System.out.println("Hakkame kohvi tegema.");
                Thread.sleep(10000);
                System.out.println("Kohvi on valmis");
                beanCount = beanCount - beansForCoffee * 2;
                waterAmount = waterAmount - waterAmountForCoffee;
            } else if (beanCount < beansForCoffee * 2) {
                System.out.println("Ei ole piisavalt kohviube, lisa kohviube!");
            } else if (waterAmount < waterAmountForCoffee) {
                System.out.println("Ei ole piisavalt vett, lisa vett!");
            }
        }
    }


    public void makeCappucchino(int amount, String[] tellija) throws InterruptedException {
        for (int i = 0; i < amount; i++) {
            System.out.println("Hakkame valmistama Cappuchinot");
            Thread.sleep(5000);
            if (beanCount > beansForCoffee) {
                System.out.println("Jahvatame ube.");
                Thread.sleep(5000);
                if (waterAmount > waterAmountForCoffee) {
                    System.out.println("Teeme vett soojaks.");
                    Thread.sleep(5000);
                    if (milk > milkForCappuccino) {
                        System.out.println("Cappuchino valmis, cappuchino tellija: " + tellija[i]);
                        missingMilk = waterAmount - waterAmountForCoffee;
                        beanCount = beanCount - beansForCoffee;
                        milk = milk - milkForCappuccino;
                        Thread.sleep(5000);
                    } else {
                        System.out.println("Lisa piima, et teha cappuchinot, puudu on: " + Math.abs(missingMilk) + " ml piima, et teha cappuchinot: " + tellija[i]);
                        break;
                    }
                } else {
                    System.out.println("Lisa vett, et teha cappuchinot, puudu on: " + Math.abs(missingWater) + "ml vett, et teha cappuchinot: " + tellija[i]);
                    break;
                }
            } else {
                System.out.println("Lisa kohviube, et teha cappuchinot, puudu on: " + Math.abs(missingBeans) + " uba, et teha cappuchinot: " + tellija[i]);
                break;
            }
        }
    }


}


