package day05;

import javax.print.DocFlavor;
import java.lang.reflect.Array;
import java.util.*;

public class ExercsisesDay05 {
    public static void main(String[] args) {
/*
//       Ülesanne 1
        System.out.println("######");
        System.out.println("#####");
        System.out.println("####");
        System.out.println("###");
        System.out.println("##");
        System.out.println("#");
*/

//       For systeem
   /*     int counter = 0;
        for (int i = 0; i < 7; i++) {
            for (int j = i; j < 7; j++) {
                counter++;
                System.out.print("#");
            }
            System.out.println();
        }*/


/*//      Ülesanne 2
            System.out.println("#");
            System.out.println("##");
            System.out.println("###");
            System.out.println("####");
            System.out.println("#####");
            System.out.println("######");*/

//
//        for (int i = 1; i < 7; i++) {
//            for (int j = 0; j < i; j++) {
//                System.out.print("#");
//            }
//            System.out.println();
//        }





/*//      Ülesanne 3
            System.out.println("@");
            System.out.println("@@");
            System.out.println("@@@");
            System.out.println("@@@@");
            System.out.println("@@@@@");*/


/*        for (int j = 5; j > 0; j--) {
            for (int i = j; i > 0; i--) {
                System.out.print(" ");
            }
            for (int l = j; l < 6; l++) {
                System.out.print("@");
            }
            System.out.println();

        }*/

/*//      Ülesanne 4
            int numberToReverse = 1234567;
            int reverseNumber = 0;
            while (numberToReverse != 0) {
                reverseNumber = reverseNumber * 10 + numberToReverse % 10;
                numberToReverse /= 10;
            }
            System.out.println(reverseNumber);*/

/*//      Ülesanne 5
        Scanner userInput = new Scanner(System.in); // võtab kasutaja hinde
        System.out.println("Sisesta nimi: ");
        String userName = userInput.next();
        System.out.println("Sisest punktide arv: ");
       Integer userGrade = userInput.nextInt();
        if (userGrade < 51) {
            System.out.println("FAIL");
        }else if (userGrade >= 51 && userGrade <= 60 ){
            System.out.println("Pass - "  + " Hinne: 1, punkti summa: "+ userGrade);
        }else if (userGrade >= 61 && userGrade <= 70 ){
            System.out.println("Pass - "  + " Hinne: 2, punkti summa: "+ userGrade);
        }else if (userGrade >= 71 && userGrade <= 80 ){
            System.out.println("Pass - "  + " Hinne: 3, punkti summa: "+ userGrade);
        }else if (userGrade >= 81 && userGrade <= 90 ){
            System.out.println("Pass - "  + " Hinne: 4, punkti summa: "+ userGrade);
        }else if (userGrade >= 91 && userGrade <= 100 ){
            System.out.println("Pass - " + " Hinne: 5, punkti summa: "+ userGrade );
        }*/
/*
//      Ülesanne 6

        double result = 0d;
        double[][] ülesanne6 = {{2.00d, 4.00d},{5.00d, 9.54d}};
        for (int i = 0; i <= ülesanne6.length - 1; i++ ) { // for (double[] sides : ülesanne6) { double result = Math.sqrt(Math.pow(sides[0], 2) + Math.pow(sides[1], 2)));
                  result = (Math.sqrt(Math.pow(ülesanne6[i][0], 2) + Math.pow(ülesanne6[i][1],2)) ) ;

            System.out.println(result);
        }*/

/*
//      Ülesanne 7

        Object[][] countryInfo = new Object[10][];
        countryInfo[0] = new Object[]{"Estonia", "Tallinn", "est", new String[]{"Estonian", "Russian","Ukrainian", "Belarusian", "Finnish"}};
        countryInfo[1] = new Object[]{"Latvia", "Riga", "lt", new String[]{"Estonian", "Russian","Ukrainian", "Belarusian", "Finnish"}};
        countryInfo[2] = new Object[]{"USA", "Washington DC", "usa",new String[]{"Estonian", "Russian","Ukrainian", "Belarusian", "Finnish"}};
        countryInfo[3] = new Object[]{"Russia", "Moscow", "rus", new String[]{"Estonian", "Russian","Ukrainian", "Belarusian", "Finnish"}};
        countryInfo[4] = new Object[]{"Lithuania", "Vilnius", "lv",new String[]{"Estonian", "Russian","Ukrainian", "Belarusian", "Finnish"}};
        countryInfo[5] = new Object[]{"Germany", "Berlin", "ge",new String[]{"Estonian", "Russian","Ukrainian", "Belarusian", "Finnish"}};
        countryInfo[6] = new Object[]{"Sweden", "Stockholm", "swe", new String[]{"Estonian", "Russian","Ukrainian", "Belarusian", "Finnish"}};
        countryInfo[7] = new Object[]{"Norway", "Oslo", "now",new String[]{"Estonian", "Russian","Ukrainian", "Belarusian", "Finnish"}};
        countryInfo[8] = new Object[]{"Finland", "Helsinki", "fin", new String[]{"Estonian", "Russian","Ukrainian", "Belarusian", "Finnish"}};
        countryInfo[9] = new Object[]{"Denmark", "Coppenhaagen", "dk",new String[]{"Estonian", "Russian","Ukrainian", "Belarusian", "Finnish"}};

*/
/*
        for (int i = 0; i < countryInfo.length; i++){
            System.out.println(countryInfo[i][2]);
        }*//*


        for (int j = 0; j < countryInfo.length; j++) {
            Object[] oaElement = countryInfo[j];
            System.out.print(countryInfo[j][0] + ":" );
            String[] languages = (String[])countryInfo[j][3];
            System.out.println("");
            for (String language : languages){
                System.out.println(language);
            }
            System.out.println("");

        }

*/


//

/*       Ülesanne 8
        List<Object> country = new ArrayList<>();
        country.add(Arrays.asList("Estonia", new ArrayList<String>(Arrays.asList("Imelik", "Russian", "Ukrainian", "Belarusian", "Finnish"))));
        country.add(Arrays.asList("Latvia", new ArrayList<String>(Arrays.asList("Estonian", "Kollane", "Ukrainian", "Belarusian", "Finnish"))));
        country.add(Arrays.asList("Italy", new ArrayList<String>(Arrays.asList("Estonian", "Russian", "Ukrainian", "Viga", "Finnish"))));
        country.add(Arrays.asList("France", new ArrayList<String>(Arrays.asList("Estonian", "Russian", "Ukrainian", "Belarusian", "Proov"))));
        country.add(Arrays.asList("USA", new ArrayList<String>(Arrays.asList("Estonian", "Russian", "UUhuu", "Belarusian", "Finnish"))));


        for (int i = 0; i < country.size(); i++) {
            List<Object> countries = (List<Object>) country.get(i);
            System.out.println(countries.get(0) + ": ");
            List<String> languages = (List<String>) countries.get(1);


            for (String language : languages) {
                System.out.println(language);
            }
            System.out.println();
        }
*/

/*//     Ülesanne 9

        Map<String, String[]> map1 = new TreeMap<>();
        map1.put("Estonia" ,new String[] {"Imelik", "Russian", "Ukrainian", "Belarusian", "Finnish"} );
        map1.put("Latvia" ,new String[] {"Estonian", "Kollane", "Ukrainian", "Belarusian", "Finnish"} );
        map1.put("Italy" ,new String[] {"Estonian", "Russian", "Ukrainian", "Viga", "Finnish"} );
        map1.put("France" ,new String[] {"Estonian", "Russian", "Ukrainian", "Belarusian", "Proov"} );
        map1.put("Usa" ,new String[] {"Estonian", "Russian", "UUhuu", "Belarusian", "Finnish"} );

        for (String key : map1.keySet()) {
            System.out.println(key + ": ");
            String[] languages = map1.get(key);
            for (int i = 0; i < languages.length; i++){
                System.out.println(languages[i]);
            }
            System.out.println();


 *//*       }

            }*/
/*

//        Ülessanne 10

        LinkedList<String[]> queue1 = new LinkedList<>();
        queue1.add(new String[]{"Estonia", "Estonian", "Russian", "UUhuu", "Belarusian", "Finnish"});
        queue1.add(new String[]{"Italy", "Russian", "Ukrainian", "Belarusian", "Proov"});
        queue1.add(new String[]{"Paris", "Estonian", "Russian", "UUhuu", "Belarusian", "Finnish"});
        queue1.add(new String[]{"Latvia", "Estonian", "Russian", "UUhuu", "Belarusian", "Finnish"});

        while (!queue1.isEmpty()) {
            String[] queu2 = queue1.pop();
            System.out.println(queu2[0]);
            for (int i = 1; i < queu2.length; i++) {
                System.out.println(queu2[i]);
            }
            System.out.println();
        }
        ;
        ;
*/


    }


}







